311118-05-xb: The lower lid of the wrist assembly. Same for right and left
311112-05-CR/L: The lid that covers the gears and upper part of the wrist
311112-05-BR/L: This part screws onto the ATI43 force sensor, the motor, attaches to the finger grips, and contains the gearing
311112-05-AR/L: The lower part of the wrist, which is press-fit onto the next joint's motor. This houses the breakout PCB and most of the motor body.